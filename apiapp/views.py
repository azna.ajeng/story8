# from django.shortcuts import render
# import requests
# from django.http import JsonResponse

# # Create your views here.
# def index(request):
#     return render(request,'index.html')

# def get_data(req):
#     key = req.GET['key']
#     print(key)
#     url = 'https://www.googleapis.com/books/v1/volumes?q=%3Ckeyword%3E' + key

#     response = requests.get(url)
#     response_json = response.json()
#     print(response)
#     #print(response_json)

#     return JsonResponse(response_json)

from django.shortcuts import render
from django.http import JsonResponse
import requests


def index(request):
    return render(request, 'index.html')

def get_data(request):
    key = request.GET['key']
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + key
    response = requests.get(url)
    response_json = response.json()

    return JsonResponse(response_json)
